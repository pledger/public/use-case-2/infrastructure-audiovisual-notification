#ifndef DENM_APPLICATION_HPP_EUIC2VFR
#define DENM_APPLICATION_HPP_EUIC2VFR

#include "application.hpp"
#include <vanetza/common/runtime.hpp>


class DenmApplication : public Application
{
public:
    DenmApplication(vanetza::Runtime& rt);
    PortType port() override;
    void indicate(const DataIndication&, UpPacketPtr) override;
    
private:
    void trigger_critical();
    void cancel_critical();
    void trigger_warning();
    void cancel_warning();


    vanetza::Runtime& runtime_;
    bool warning_;
    bool critical_;
};

#endif /* CAM_APPLICATION_HPP_EUIC2VFR */
